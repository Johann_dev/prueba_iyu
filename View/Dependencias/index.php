<!--
 * Datos de autor
 * nombre: Johann Esneider
 * e-mail: johannesneider.dev@gmail.com
 * última modificación: 17 Mayo 2021
-->

<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8" />
    <title>Módulo dependencias - IYU</title>
    <!-- inicio - Estilo general -->
    <link rel="stylesheet" href="../../Resources/css/general_style.css" style="text/css">
    <!-- fin - Estilo general -->
    <!-- inicio Bootstrap -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- fin Bootstrap-->
    <!-- inicio material icons -->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!-- fin material icons -->
    <!--  Fonts -->
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@300&display=swap" rel="stylesheet">
    <link href="//cdn.datatables.net/1.10.24/css/jquery.dataTables.min.css" rel="stylesheet">
    <!-- fin Fonts -->
</head>
<body class="content">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="../../index.php">Inicio</a></li>
            <li class="breadcrumb-item active" aria-current="page"><b>Dependencias</b></li>
        </ol>
    </nav>
    <section class="container" style="text-align:center;">
        <fieldset>
            <legend>Módulo de dependencias</legend>
            <br>
            <div class="form-group">
            <label for="buscar">Seleccione una dependencia</label>
            <select name="dep_code" id="dep_code" class="form-control" onChange="active_btn();">
                <!-- opciones vienen por Ajax -->
            </select>
        </div>
        <div class="form-group">
            <input type="button" class="btn btn-primary btn-block" id="search_btn" value="Buscar datos" onClick="search_dep();" disabled />
        </div>
        <table class="table table-bordered table-sm" id="dependencies_datatable">
            <thead>
                <tr>
                    <th colspan="5">Datos del usuario</th>
                </tr>
                <tr>
                    <th>Nombre</th>
                    <th>Usuario</th>
                    <th>Dependencia</th>
                    <th>Perfil</th>
                    <th>Estado</th>
                </tr>
            </thead>
            <tbody id="dep_tab"></tbody>
        </table>
        </fieldset>
        <br>
        <footer>
            <small class="footer">Developed by Johann Esneider&copy; - Prueba técnica IYU</small>
        </footer>        
    </section>
    <!-- inicio Bootstrap -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <!-- fin Bootstrap -->
    <!-- jQuery -->
    <script src="http://code.jquery.com/jquery-1.9.1.js"></script>
    <script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <!-- fin jQuery -->
    <!-- Inicio Sweet alert 2-->
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <!-- Fin Sweet alert 2-->
    <!-- Inicio Datatable-->
    <script src="//cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/plug-ins/a5734b29083/i18n/Spanish.json"></script>
    <!-- Fin Datatable-->
    <script src="../../Resources/js/dependencia.js" type="text/javascript"></script>
</body>
</html>