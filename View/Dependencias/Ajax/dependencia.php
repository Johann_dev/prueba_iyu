<?php

session_start();
class ajax
{

    // conexión a la base
    public function conectar()
    {
        $file_conn="../../../Config/config.ini";
		$conn_data = parse_ini_file($file_conn,true);

        $conn_string = "host=".$conn_data["host"]." port=".$conn_data["port"]." dbname=".$conn_data["database"]." user=".$conn_data["user"]." password=".$conn_data["password"]." options='--client_encoding=UTF8'";
        $dbconn = pg_connect($conn_string);

        return $dbconn;
    }

    public function get_dependencies(){

        $conn = $this->conectar();

        $data = [];

        $sql = "SELECT * FROM dependencia ORDER BY id ASC";
        $res = pg_query($conn, $sql);

        $options = '<option value="">Seleccione una dependencia</option>';
        while($row = pg_fetch_assoc($res)){
            $options .= '<option value="'.$row["depe_codi"].'">'.$row["depe_codi"].' - '.$row["depe_nomb"].'</option>';
        }
        
        return $options;
    }

    public function get_users($dep_code){
        $conn = $this->conectar();

        $data= [];

        $sql = "SELECT * FROM usuario WHERE depe_codi = '".$dep_code."'";
        $res = pg_query($conn, $sql);
        while($row = pg_fetch_array($res)){

            $sql_dep = "SELECT depe_nomb FROM dependencia WHERE depe_codi = '".$row['depe_codi']."'";
            $res_dep = pg_query($conn, $sql_dep);
            $row_dep = pg_fetch_assoc($res_dep);

            $row['nombre_dependencia'] = $row_dep['depe_nomb'];

            if($row['usua_codi'] == '1'){
                $row['perfil'] = 'Jefe';
            }else{
                $row['perfil'] = 'Normal';
            }

            if($row['usua_esta'] == '1'){
                $row['user_state'] = 'Activo';
            }else if($row['usua_esta'] == 0){
                $row['user_state'] = 'Inactivo';
            }

            $data[] = $row;
        }

        return $data;
    }
}

//Crea el objeto de la clase ajax
$obj = new ajax;


$action = $_REQUEST['action'];

if ($action == 'get_dependencies') {
        
    $detail = $obj->get_dependencies();
    echo $detail;
}else if($action == 'get_users'){
    $detail = $obj->get_users($_POST['dependencia']);
    echo json_encode($detail);
}

